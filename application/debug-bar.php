<?php

global $wpdb;

$bb = BlackBox::getInstance();
$time = number_format($bb->getProfiler()->totalTime()*1000, 2);
$sqlC = count($wpdb->queries);
$sqlT = 0;
$err = count($bb->getErrors());
$errI = 0;

foreach($wpdb->queries as $q) {
	$sqlT += $q[1];
}

foreach($bb->getErrors() as $error) {
	if($error['errno'] == E_WARNING) {
		$errI++;
	}
}

?>

<div id="blackbox-web-debug">
	<a href="javascript:WpDebugBar.switchPanel('globals')" class="globals">Globals</a>
	<a href="javascript:WpDebugBar.switchPanel('profiler')" class="profiler">Profiler (<?= $time; ?> ms)</a>
	<a href="javascript:WpDebugBar.switchPanel('database')" class="database">SQL (<span class="qnum"><?= $sqlC; ?></span> queries in <span class="qtime"><?= number_format( $sqlT * 1000, 2 ); ?></span> ms)</a>
	<?php if( class_exists( '\GeminiLabs\Castor\Application' )) : ?>
	<a href="javascript:WpDebugBar.switchPanel('templates')" class="templates">Templates</a>
	<?php endif; ?>
	<a href="javascript:WpDebugBar.switchPanel('errors')" class="errors">Errors (<?php echo $err; if($errI>0) echo ", $errI!" ?>)</a>
	<a href="#" class="toggle off">Toggle</a>
	<a href="javascript:WpDebugBar.close()" class="close">Close</a>

	<div id="blackbox-globals" class="debug-panel">
		<pre><code class="php">$_GET = <?= esc_html( var_export( $bb->getGlobal( 'get' ), true )); ?>;</code></pre><br/>
		<pre><code class="php">$_POST = <?= esc_html( var_export( $bb->getGlobal( 'post' ), true )); ?>;</code></pre><br />
		<pre><code class="php">$_COOKIE = <?= esc_html( var_export( $bb->getGlobal( 'cookie' ), true )); ?>;</code></pre><br />
		<pre><code class="php">$_SESSION = <?= esc_html( var_export( $bb->getGlobal( 'session' ), true )); ?>;</code></pre><br />
		<pre><code class="php">$_SERVER = <?= esc_html( var_export( $bb->getGlobal( 'server' ), true )); ?>;</code></pre><br />
	</div>

	<div id="blackbox-profiler" class="debug-panel">
		<table>
			<tbody>
			<?php $x = $bb->getProfiler()->getInit(); ?>
			<?php foreach( $bb->getProfiler()->getMeasure() as $time ) : ?>
				<tr>
					<td><?= ($time['name']); ?></td>
					<td class="number"><?= number_format( round(( $time['time'] - $x ) * 1000, 4 ), 4 ); ?> ms</td>
					<td><?= round( $time['memory'] / 1000 ); ?> kB</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<div id="blackbox-database" class="debug-panel">
		<form action="" method="get" class="blackbox-filter">
			<label for="bb_query_filter">Find queries containing</label>
			<input type="text" name="bb_query_filter" id="bb_query_filter" class="no-label" value="" />
			<label for="bb_query_min_time">Min. Execution Time</label>
			<input type="text" name="bb_query_min_time" id="bb_query_min_time" class="no-label" value="" />
		</form>
		<table>
			<tbody>
				<?php foreach( $wpdb->queries as $q ) : ?>
				<tr>
					<td class="number">
						<?= number_format( round( $q[1] * 1000, 4 ), 4 ); ?> [ms]
					</td>
					<td>
					<?php
						$q[0] = str_replace( [PHP_EOL, '  '], ' ', $q[0] );
					?>
						<pre><code class="sql"><?= str_replace([
							'FROM',
							'INNER JOIN',
							'WHERE',
							'AND',
							'LIMIT',
							'ORDER BY'
						], [
							PHP_EOL.'FROM',
							PHP_EOL.'INNER JOIN',
							PHP_EOL.'WHERE',
							PHP_EOL.'AND',
							PHP_EOL.'LIMIT',
							PHP_EOL.'ORDER BY',
						], $q[0] ); ?></code></pre>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<?php if( class_exists( '\GeminiLabs\Castor\Application' )) : ?>
	<div id="blackbox-templates" class="debug-panel">
		<?php Development::printTemplatePaths(); ?>
	</div>
	<?php endif; ?>

	<div id="blackbox-errors" class="debug-panel">
		<table>
			<tbody>
				<?php foreach($bb->getErrors() as $error): ?>
				<tr>
					<td class="err-name">
						<span <?php if($error['errno'] == E_WARNING): ?>style="color:red"<?php endif; ?>><?php echo $error['name'] ?></span>
						<?php if($error['count']>1): ?> (<?php echo $error['count'] ?>)<?php endif; ?></td>
					<td><?php echo $error['message'] ?> on line <?php echo $error['line']; ?> in file <?php echo $error['file'] ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
