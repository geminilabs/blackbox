<?php

/**
 * Main BlackBox class
 *
 * @author Grzegorz Winiarski
 * @package BlackBox
 * @license GPL
 */

class BlackBox
{
	/**
	 * Filter name to trace
	 */
	const DEBUG = "debug";

	/**
	 * (Singleton) Insatnce of BlackBox object
	 *
	 * @var BlackBox
	 */
	private static $_instance = null;

	/**
	 * List of globals
	 *
	 * Globals will be stored in the array in case someone will want to
	 * modify them
	 *
	 * @var array
	 */
	private $_globals = [
		'cookie'  => null,
		'get'     => null,
		'post'    => null,
		'server'  => null,
		'session' => null,
	];

	/**
	 * Profiler object
	 *
	 * @var BlackBox_Profiler
	 */
	private $_profiler = null;

	/**
	 * List of catched errors by BlackBox error handler.
	 *
	 * @var BlackBox_Error[]
	 */
	private $_error = array();

	/**
	 *
	 * @var string
	 */
	private $_path = null;

	private function __construct()
	{
		$this->_profiler = new BlackBox_Profiler();
		$this->_globals = [
			'cookie'  => isset( $_COOKIE ) ? $_COOKIE : [],
			'get'     => isset( $_GET ) ? $_GET : [],
			'post'    => isset( $_POST ) ? $_POST : [],
			'server'  => isset( $_SERVER ) ? $_SERVER : [],
			'session' => isset( $_SESSION ) ? $_SESSION : [],
		];
	}

	/**
	 * Returns profiler object
	 *
	 * @return BlackBox_Profiler
	 */
	public function getProfiler()
	{
		return $this->_profiler;
	}

	/**
	 * Returns all global arrays copies
	 *
	 * @return array
	 */
	public function getGlobals()
	{
		return $this->_globals;
	}

	/**
	 * Returns selected (by $name) global array
	 *
	 * @param string $name Global array name
	 * @return array
	 * @throws BlackBox_Exception If global array does not exists
	 */
	public function getGlobal( $name )
	{
		$name = strtolower( $name );
		if( !array_key_exists( $name, $this->_globals )) {
			throw new BlackBox_Exception( "Global $name does not exist.", 100 );
		}
		return $this->_globals[$name];
	}

	/**
	 * Retuns BlackBox instance.
	 *
	 * Note. There can be only one instance of BlackBox object.
	 *
	 * @return BlackBox
	 */
	public static function getInstance()
	{
		if( self::$_instance === null ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public static function errorHandler( $errno, $errstr, $errfile, $errline )
	{
		$errorCodes = [
			2 => "Warning",
			8 => "Notice",
			2048 => "Strict",
			8192 => "Deprecated",
		];

		$errname = array_key_exists( $errno, $errorCodes )
			? $errorCodes[$errno]
			: "Unknown";

		$hash = md5( $errline . $errfile . $errstr . $errno );

		if( array_key_exists( $hash, self::getInstance()->_error )) {
			self::getInstance()->_error[$hash]['count']++;
		}
		else {
			self::getInstance()->_error[$hash] = [
				"errno"   => $errno,
				"message" => $errstr,
				"file"    => $errfile,
				"name"    => $errname,
				"line"    => $errline,
				"count"   => 0,
			];
		}
	}

	/**
	 * Returns all errors catched by {@see self::errorHandler()} method.
	 *
	 * @return array
	 */
	public function getErrors()
	{
		return $this->_error;
	}

	/**
	 * Initiates BlackBox plugin
	 *
	 * @return null
	 */
	public static function init()
	{
		// init profiler
		apply_filters( 'debug', 'Profiler Initiated' );
		apply_filters( 'debug', 'Profiler Noise' );

		add_action( 'init',         ['BlackBox',      'init_scripts_styles'] );
		add_action( 'admin_footer', ['BlackBox_Hook', 'footer'] );
		add_action( 'wp_footer',    ['BlackBox_Hook', 'footer'] );
		add_filter( 'all',          ['BlackBox_Hook', 'profiler'] );

		set_error_handler( ['BlackBox', 'errorHandler'], E_ALL | E_STRICT );
	}

	public static function init_scripts_styles()
	{
		$dir = plugins_url( '/public/', dirname( __FILE__ ));

		wp_enqueue_style( 'highlight-css', $dir . 'styles/atom-one-dark.css' );
		wp_enqueue_style( 'blackbox-css', $dir . 'styles.css', ['dashicons'] );

		wp_enqueue_script( 'highlight-js', $dir . 'highlight.pack.js' );
		wp_enqueue_script( 'blackbox-js', $dir . 'blackbox.js' );
	}
}

