hljs.initHighlightingOnLoad();

var WpDebugBar =
{
	element: ['globals', 'profiler', 'database', 'templates', 'errors'],

	open: null,

	switchPanel: function(open) {

		for(var i in WpDebugBar.element) {
			var el = document.getElementById("blackbox-"+WpDebugBar.element[i]);
			if( el ) {
				el.style.display = "none";
				document.querySelector('a.'+WpDebugBar.element[i]).classList.remove( 'active' );
			}
		}

		if(open == WpDebugBar.open) {
			WpDebugBar.open = null
			return;
		}

		WpDebugBar.open = open;
		document.getElementById("blackbox-"+open).style.display = "block";
		document.querySelector('a.'+open).classList.add( 'active' );

	},

	close: function() {
		document.getElementById("blackbox-web-debug").style.display = "none";
	},

	createCookie: function (name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	},

	readCookie: function(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	},

	eraseCookie: function(name) {
		createCookie(name,"",-1);
	},
}

document.addEventListener('DOMContentLoaded', function() {

	var debugToggle = document.querySelector( '#blackbox-web-debug a.toggle' );
	var debugFilter = document.querySelector( '#bb_query_filter' );
	var debugMinTime = document.querySelector( '#bb_query_min_time' );

	if( !debugToggle )return;

	var onClick = function( ev ) {
		var toggle = ev.target.closest( 'a' );
		var panel = toggle.closest( '#blackbox-web-debug' );
		if( toggle.classList.contains( 'off' )) {
			toggle.classList.remove( 'off' );
			toggle.classList.add( 'on' );
			toggle.textContent = '';
			panel.classList.add( 'mini' );
			panel.querySelectorAll( '.debug-panel' ).forEach( function( el ) {
				el.style.display = 'none';
			});
			panel.querySelectorAll( 'a.active' ).forEach( function( el ) {
				el.classList.remove( 'active' );
			});
			WpDebugBar.createCookie( 'bb_toggle', 'on' );
		}
		else {
			toggle.classList.remove( 'on' );
			toggle.classList.add( 'off' );
			toggle.textContent = 'Toggle';
			panel.classList.remove( 'mini' );
			WpDebugBar.createCookie( 'bb_toggle', 'off' );
		}
		ev.preventDefault();
	};

	var onKeyup = function( ev ) {
		var time = parseFloat( debugMinTime.value );
		var query = debugFilter.value;
		var qnum = 0;
		var qtime = 0;

		document.querySelectorAll( '#blackbox-database tr' ).forEach( function( tr ) {
			var minTimeFilter = parseFloat( tr.querySelector( '.number' ).textContent.replace( ' [ms]', '' ));
			var queryFilter = tr.querySelector( '.sql' ).textContent.indexOf( query );
			var timeResult = time > 0 && minTimeFilter < time;
			var queryResult = query.length > 0 && queryFilter == -1;

			if( timeResult || queryResult ) {
				tr.style.display = 'none';
			}
			else {
				tr.style.display = '';
				qnum++;
				qtime += minTimeFilter;
			}
		});

		document.querySelector( '.qnum' ).textContent = qnum;
		document.querySelector( '.qtime' ).textContent = qtime.toFixed(2);

		WpDebugBar.createCookie( 'bb_query_filter', query );
		WpDebugBar.createCookie( 'bb_query_min_time', document.querySelector( '#bb_query_min_time' ).value );
	};

	debugToggle.addEventListener( 'click', onClick );
	debugFilter.addEventListener( 'keyup', onKeyup );
	debugMinTime.addEventListener( 'keyup', onKeyup );

	// init
	if( WpDebugBar.readCookie( 'bb_toggle' ) === 'on' ) {
		debugToggle.click;
	}

	debugFilter.value = WpDebugBar.readCookie( 'bb_query_filter' );
	debugMinTime.value = WpDebugBar.readCookie( 'bb_query_min_time' );

	// $("#bb_query_filter").keyup();
});
